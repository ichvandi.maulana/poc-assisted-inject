package com.vandoc.pocassistedinjection

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 08:11.
 */
@HiltAndroidApp
class MainApp : Application()
