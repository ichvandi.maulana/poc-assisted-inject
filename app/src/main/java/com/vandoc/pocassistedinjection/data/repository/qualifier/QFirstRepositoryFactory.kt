package com.vandoc.pocassistedinjection.data.repository.qualifier

import javax.inject.Qualifier

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 10:14.
 */
@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class QFirstRepositoryFactory
