package com.vandoc.pocassistedinjection.data.datasource.qualifier

import javax.inject.Qualifier

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 08:17.
 */
@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class QFirstDataSource
