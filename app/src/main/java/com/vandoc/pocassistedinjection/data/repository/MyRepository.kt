package com.vandoc.pocassistedinjection.data.repository

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 10:07.
 */
interface MyRepository {
    fun getString(): String

    interface Factory {
        fun create(isFirst: Boolean): MyRepository
    }

    companion object {
        fun create(factory: Factory, isFirst: Boolean): MyRepository {
            return factory.create(isFirst)
        }
    }
}
