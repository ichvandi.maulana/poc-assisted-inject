package com.vandoc.pocassistedinjection.data.repository.impl

import com.vandoc.pocassistedinjection.data.datasource.DataSourceFactory
import com.vandoc.pocassistedinjection.data.repository.MyRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 08:19.
 */
class SecondRepository @AssistedInject constructor(
    dataSourceFactory: DataSourceFactory.Factory,
    @Assisted
    private val isFirst: Boolean,
) : MyRepository {
    private val dataSource = dataSourceFactory.create(isFirst).getDataSource()

    override fun getString(): String = "${this::class.java.simpleName} + ${dataSource.getString()}"

    @AssistedFactory
    interface Factory : MyRepository.Factory {
        override fun create(isFirst: Boolean): SecondRepository
    }
}
