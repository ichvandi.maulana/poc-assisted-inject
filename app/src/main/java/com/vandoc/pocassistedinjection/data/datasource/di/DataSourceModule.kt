package com.vandoc.pocassistedinjection.data.datasource.di

import com.vandoc.pocassistedinjection.data.datasource.MyDataSource
import com.vandoc.pocassistedinjection.data.datasource.impl.FirstDataSource
import com.vandoc.pocassistedinjection.data.datasource.impl.SecondDataSource
import com.vandoc.pocassistedinjection.data.datasource.qualifier.QFirstDataSource
import com.vandoc.pocassistedinjection.data.datasource.qualifier.QSecondDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 08:14.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    @QFirstDataSource
    abstract fun bindFirstDataSource(impl: FirstDataSource): MyDataSource

    @Binds
    @QSecondDataSource
    abstract fun bindSecondDataSource(impl: SecondDataSource): MyDataSource
}
