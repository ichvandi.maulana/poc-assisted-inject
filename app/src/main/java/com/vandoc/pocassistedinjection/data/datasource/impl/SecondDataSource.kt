package com.vandoc.pocassistedinjection.data.datasource.impl

import com.vandoc.pocassistedinjection.data.datasource.MyDataSource
import javax.inject.Inject

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 08:13.
 */
class SecondDataSource @Inject constructor() : MyDataSource {
    override fun getString(): String {
        return this::class.java.simpleName
    }
}
