package com.vandoc.pocassistedinjection.data.repository.di

import com.vandoc.pocassistedinjection.data.repository.MyRepository
import com.vandoc.pocassistedinjection.data.repository.impl.FirstRepository
import com.vandoc.pocassistedinjection.data.repository.impl.SecondRepository
import com.vandoc.pocassistedinjection.data.repository.qualifier.QFirstRepositoryFactory
import com.vandoc.pocassistedinjection.data.repository.qualifier.QSecondRepositoryFactory
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 10:09.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @QFirstRepositoryFactory
    abstract fun bindFirstRepositoryFactory(impl: FirstRepository.Factory): MyRepository.Factory

    @Binds
    @QSecondRepositoryFactory
    abstract fun bindSecondRepositoryFactory(impl: SecondRepository.Factory): MyRepository.Factory
}
