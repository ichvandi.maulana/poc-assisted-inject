package com.vandoc.pocassistedinjection.data.datasource

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 08:12.
 */
interface MyDataSource {
    fun getString(): String
}
