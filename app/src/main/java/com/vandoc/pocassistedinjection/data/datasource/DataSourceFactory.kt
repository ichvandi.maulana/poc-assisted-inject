package com.vandoc.pocassistedinjection.data.datasource

import com.vandoc.pocassistedinjection.data.datasource.qualifier.QFirstDataSource
import com.vandoc.pocassistedinjection.data.datasource.qualifier.QSecondDataSource
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

/**
 * @author Ichvandi
 * Created on 27/10/2023 at 08:48.
 */
class DataSourceFactory @AssistedInject constructor(
    @QFirstDataSource
    private val firstDataSource: MyDataSource,
    @QSecondDataSource
    private val secondDataSource: MyDataSource,
    @Assisted
    private val isFirst: Boolean,
) {
    fun getDataSource() = if (isFirst) firstDataSource else secondDataSource

    @AssistedFactory
    interface Factory {
        fun create(isFirst: Boolean): DataSourceFactory
    }
}
