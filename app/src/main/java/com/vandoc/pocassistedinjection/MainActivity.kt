package com.vandoc.pocassistedinjection

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vandoc.pocassistedinjection.data.repository.MyRepository
import com.vandoc.pocassistedinjection.data.repository.qualifier.QFirstRepositoryFactory
import com.vandoc.pocassistedinjection.data.repository.qualifier.QSecondRepositoryFactory
import com.vandoc.pocassistedinjection.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.random.Random

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    @Inject
    @QFirstRepositoryFactory
    lateinit var firstRepositoryFactory: MyRepository.Factory

    @Inject
    @QSecondRepositoryFactory
    lateinit var secondRepositoryFactory: MyRepository.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val myRepository = MyRepository.create(
            if (Random.nextBoolean()) firstRepositoryFactory else secondRepositoryFactory,
            Random.nextBoolean(),
        )

        binding.btnGenerate.setOnClickListener {
            binding.tvTitle.text = myRepository.getString()
        }
    }
}
